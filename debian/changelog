libclass-handle-perl (1.07-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libclass-handle-perl: Add :any qualifier for perl dependency.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 17 Dec 2022 16:59:04 +0000

libclass-handle-perl (1.07-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Set upstream metadata fields: Contact, Name.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 10 Jun 2022 22:20:18 +0100

libclass-handle-perl (1.07-3) unstable; urgency=medium

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Add Testsuite header for pkg-perl-autopkgtest.

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build
  * Re-add accidentally dropped libclass-isa-perl to dependencies

 -- Niko Tyni <ntyni@debian.org>  Sat, 10 Feb 2018 10:52:30 +0200

libclass-handle-perl (1.07-2) unstable; urgency=low

  * Team upload.
  * Depend on libclass-isa-perl | perl (<< 5.10.1-13).  Closes: #614408
  * Update the copyright to the latest DEP 5 candidate format.
  * Bump the debhelper compatibility level to 8 with no changes.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 24 Feb 2011 23:48:20 +0200

libclass-handle-perl (1.07-1) unstable; urgency=low

  [ Jonathan Yu ]
  * Initial Release. (Closes: #598026)

 -- gregor herrmann <gregoa@debian.org>  Sat, 25 Sep 2010 16:01:04 +0200
